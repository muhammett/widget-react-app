import { WIDGET_DELETED, WIDGET_CREATED_ERROR, WIDGET_CREATED, FETCH_NEW_USER_WIDGETS, USER_REGISTER_ERROR, SET_CURRENT_USER, FETCH_VISIBLE_WIDGETS, LOGIN_FAILED, FETCH_NEW_USER } from './types'

import axios from 'axios';

const BASE_URL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3001/v1' : 'http://showoff-api.muhammet.dev/v1'

export function setAuthorizationToken(token) {
  if (token) {
    axios.defaults.headers.common['Authorization'] = `${token}`;
    console.log('TOKEN SET')
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
}

export const createUser = (data) => dispatch => {
  axios.post(`${BASE_URL}/users`, data).then(function (response) {

    const token = response.data.data.access_token;

    localStorage.setItem('AUTH_TOKEN', token);

    setAuthorizationToken(token);

    dispatch(setCurrentUser(response.data.data));

  }).catch(function (error) {
    dispatch({
      type: USER_REGISTER_ERROR,
      payload: error.response.data.errors
    })
  });
};

export const fetchWidgets = (query) => dispatch => {

  query = query == null || query === undefined ? '' : query

  axios.get(`${BASE_URL}/widgets?q=${query}`)
    .then(response =>
      dispatch({
        type: FETCH_VISIBLE_WIDGETS,
        payload: response.data.data
      })
    );
};

export function setCurrentUser(user) {
  console.log('setCurrentUser called')
  return {
    type: SET_CURRENT_USER,
    payload: user,
  };
}

export function fetchCurrentUser() {
  console.log('fetchCurrentUser called')
  return axios.get(`${BASE_URL}/users/me`, {});
}

export const logout = () => dispatch => {
  localStorage.removeItem('AUTH_TOKEN');
  setAuthorizationToken(false);
  dispatch(setCurrentUser({}));
};

export const createSession = (data) => dispatch => {
  axios.post(`${BASE_URL}/sessions`, data).then(function (response) {

    const token = response.data.data.access_token;

    localStorage.setItem('AUTH_TOKEN', token);

    setAuthorizationToken(token);

    fetchCurrentUser().then(function (response) {
      console.log(response)
      dispatch(setCurrentUser(response.data.data))
    }).catch(function (error) {
      console.log(error);
    });

  }).catch(function (error) {
    dispatch({
      type: LOGIN_FAILED,
      payload: error.response.data.errors
    })
  });
};

export const fetchUser = (id) => dispatch => {
  axios.get(`${BASE_URL}/users/${id}`, {}).then(function (response) {

    const data = response.data.data;

    dispatch({
      type: FETCH_NEW_USER,
      payload: data
    })

  }).catch(function (error) {
    console.log(error)
  });
};


export const fetchUserWidgets = (user_id, query) => dispatch => {

  query = query == null || query === undefined ? '' : query

  axios.get(`${BASE_URL}/users/${user_id}/widgets?q=${query}`)
    .then(response =>
      dispatch({
        type: FETCH_NEW_USER_WIDGETS,
        payload: response.data.data
      })
    );
};

export const createWidget = (data) => dispatch => {
  axios.post(`${BASE_URL}/widgets`, data).then(function (response) {
    dispatch({
      type: WIDGET_CREATED,
      payload: response.data.data
    });
  }).catch(function (error) {
    dispatch({
      type: WIDGET_CREATED_ERROR,
      payload: error.response.data.errors
    })
  });
};


export const destroyWidget = (id) => dispatch => {
  axios.delete(`${BASE_URL}/widgets/${id}`, {}).then(function (response) {
    dispatch({
      type: WIDGET_DELETED,
      payload: id,
    });
  }).catch(function (error) {
    console.log(error);
  });
};