
import React, { Component } from 'react';

import { Button, Modal } from 'react-bootstrap'

import { connect } from 'react-redux';

import Register from '../Users/Register'


class SignupButton extends Component {

  constructor(props) {
    super(props);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <>
        <Button variant="primary" onClick={this.handleShow}>
          Sign Up
        </Button>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Register</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Register />
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, null)(SignupButton);