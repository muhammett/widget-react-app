import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchWidgets } from '../../actions/actions';

import Widget from './Widget'

class Widgets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
    };

    this.search = this.search.bind(this);
  }

  search(e) {
    this.setState({ query: e.target.value })

    this.props.fetchWidgets(this.state.query);
  }

  componentWillMount() {
    this.props.fetchWidgets(null);
  }

  render() {
    const widgetItems = this.props.widgets.map(widget => (
      <div key={`widget-item-${widget.id}`} className="col-lg-4 col-sm-6 mb-3">
        <Widget widget={widget} />
      </div>
    ));
    return (
      <div>
        <input
          type="text"
          name="query"
          className="form-control form-control-lg mb-3"
          placeholder="Type something..."
          onChange={this.search}
          value={this.state.query}
        />
        <div className="row">
          {widgetItems}
        </div>
      </div>
    );
  }
}

Widgets.propTypes = {
  fetchWidgets: PropTypes.func.isRequired,
  widgets: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  widgets: state.widgets.items,
});

export default connect(mapStateToProps, { fetchWidgets })(Widgets);