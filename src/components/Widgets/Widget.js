import React, { Component } from 'react';

import { Link } from 'react-router-dom'

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { destroyWidget } from '../../actions/actions';
class Widget extends Component {
  constructor(props) {
    super(props);

    this.destroy = this.destroy.bind(this);
  }

  destroy(e) {
    e.preventDefault();
    this.props.destroyWidget(this.props.widget.id);
  }

  render() {
    const path = `/users/${this.props.widget.user.id}`

    const actionButtons = (
      <div>
        <a href="" onClick={this.destroy} className="btn btn-danger btn-sm">Destroy</a>
      </div>
    )

    return (
      <div className="card card-default">
        <div className="card-header">
          {this.props.widget.name}
        </div>

        <div className="card-body">
          <p>
            <Link to={path}>
              <img src={this.props.widget.user.profile_pic} width="20" />
              {this.props.widget.user.first_name}
            </Link>
          </p>

          {this.props.widget.owner ? actionButtons : null}

        </div>
      </div>
    );
  }
}

Widget.propTypes = {
  destroyWidget: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, { destroyWidget })(Widget);