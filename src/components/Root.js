import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import App from './App'

import NavigationBar from './NavigationBar'

import UserShow from './Users/Show'

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <NavigationBar />
      <div className="container">


        <Route exact path="/" component={App} />
        <Route path="/users/:id" component={UserShow} />

      </div>
    </Router>
  </Provider >
)

Root.propTypes = {
  store: PropTypes.object.isRequired
}

export default Root