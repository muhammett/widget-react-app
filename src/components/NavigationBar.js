import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { logout } from '../actions/actions';

import SignupButton from './Buttons/SignupButton'

import LoginButton from './Buttons/LoginButton'

import { Link } from 'react-router-dom'
class NavigationBar extends Component {

  logout(e) {
    e.preventDefault();
    this.props.logout();
  }

  render() {
    const userLinks = (
      <>
        <nav className="my-2 my-md-0 mr-md-3">
          <Link to="/users/me">
            My Profile
          </Link>
          <span className="navbar-text">
            Welcome
      {this.props.currentUser.first_name}
          </span>
        </nav>
        <a className="btn btn-outline-primary" href="#" onClick={this.logout.bind(this)}>Log Out</a>
      </>
    );

    const guestLinks = (
      <>
        <nav className="my-2 my-md-0 mr-md-3">
          <LoginButton />
          <SignupButton />
        </nav>
      </>
    );

    return (
      <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">

        <h5 className="my-0 mr-md-auto font-weight-normal">
          <Link to="/">
            Widget React APP</Link>
        </h5>

        {this.props.authenticated ? userLinks : guestLinks}
      </div>
    );
  }
}

NavigationBar.propTypes = {
  logout: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired,
  currentUser: PropTypes.object,
};

const mapStateToProps = state => ({
  authenticated: state.currentUser.authenticated,
  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps, { logout })(NavigationBar);