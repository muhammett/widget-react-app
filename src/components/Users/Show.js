import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUser, fetchUserWidgets } from '../../actions/actions';

import Widget from '../Widgets/Widget'

import NewWidget from './NewWidget'

class UserShow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
    };

    this.searchInWidgets = this.searchInWidgets.bind(this);
  }

  componentWillMount() {
    console.log('user detail')
    console.log(this.props.match.params.id)
    this.props.fetchUser(this.props.match.params.id);
    this.props.fetchUserWidgets(this.props.match.params.id);
  }

  searchInWidgets(e) {
    this.setState({ query: e.target.value })

    this.props.fetchUserWidgets(this.props.match.params.id, this.state.query);
  }

  render() {

    const widgetItems = this.props.widgets.map(widget => (
      <div key={`widget-item-${widget.id}`} className="col-lg-4 col-sm-6 mb-3">
        <Widget widget={widget} />
      </div>
    ));

    return (
      <div className="row">
        <div className="col-lg-4 col-sm-6">
          <div className="text-center">
            <img src={this.props.user && this.props.user.profile_pic} className="rounded-circle" width="200" />
            <h1>{this.props.user && this.props.user.first_name}</h1>
          </div>

        </div>
        <div className="col-lg-8 col-sm-6">
          {this.props.user.id == this.props.currentUser.id && <NewWidget />}



          <input
            type="text"
            name="query"
            className="form-control form-control-lg mb-3"
            placeholder="Type something..."
            onChange={this.searchInWidgets}
            value={this.state.query}
          />

          <div className="row">
            {widgetItems}
          </div>
        </div>
      </div>
    );
  }
}

UserShow.propTypes = {
  fetchUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.userProfile.user,
  widgets: state.userProfile.widgets,
  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps, { fetchUser, fetchUserWidgets })(UserShow);