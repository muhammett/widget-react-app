import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { createWidget } from '../../actions/actions';

class NewWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      kind: 'visible',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const data = {
      name: this.state.name,
      description: this.state.description,
      kind: this.state.kind,
    };

    this.props.createWidget(data)
  }

  render() {

    const error = (
      <div className="alert alert-danger" >
        {this.props.errors}
      </div>
    )

    const form = (
      <div className="card card-default">

        <div className="card-header">
          New Widget
        </div>

        <div className="card-body">
          {this.props.errors.length > 0 && error}

          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label for="widget_name">Name</label>
              <input
                type="text"
                id="widget_name"
                name="name"
                className="form-control"
                placeholder="Name"
                onChange={this.onChange}
                value={this.state.name}
              />
            </div>
            <div className="form-group">
              <label for="widget_descriotion">Description</label>
              <textarea
                type="text"
                id="widget_descriotion"
                name="description"
                className="form-control"
                placeholder="Description"
                onChange={this.onChange}
                value={this.state.description}
              />
            </div>

            <button type="submit" className="btn btn-primary btn-block">Submit</button>
          </form>
        </div>
      </div>
    )

    return (
      <div>
        {form}
      </div>
    );
  }
}

NewWidget.propTypes = {
  createWidget: PropTypes.func.isRequired,
  errors: PropTypes.array,
};

const mapStateToProps = state => ({
  errors: state.userProfile.widget_create_errors,
});

export default connect(mapStateToProps, { createWidget })(NewWidget);