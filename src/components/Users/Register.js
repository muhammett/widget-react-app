import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { createUser } from '../../actions/actions';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password,
    };

    this.props.createUser(data)
  }

  render() {

    const error = (
      <div className="alert alert-danger" >
        {this.props.errors}
      </div>
    )

    const form = (
      <div>

        {this.props.errors.length > 0 && error}

        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label for="signup_first_name">First Name</label>
            <input
              type="text"
              id="signup_first_name"
              name="first_name"
              className="form-control"
              placeholder="First Name"
              onChange={this.onChange}
              value={this.state.first_name}
            />
          </div>
          <div className="form-group">
            <label for="signup_last_name">Last Name</label>
            <input
              type="text"
              id="signup_last_name"
              name="last_name"
              className="form-control"
              placeholder="Last Name"
              onChange={this.onChange}
              value={this.state.last_name}
            />
          </div>
          <div className="form-group">
            <label for="signup_email">Email</label>
            <input
              type="text"
              id="signup_email"
              name="email"
              className="form-control"
              placeholder="Email"
              onChange={this.onChange}
              value={this.state.email}
            />
          </div>

          <div className="form-group">
            <label for="signup_password">Password</label>
            <input
              type="password"
              id="signup_password"
              name="password"
              className="form-control"
              placeholder="Password"
              onChange={this.onChange}
              value={this.state.password}
            />
          </div>
          <button type="submit" className="btn btn-primary btn-block">Submit</button>
        </form>
      </div>
    )

    const alreadyRegistered = (
      <p> Alread Registered </p>
    )

    return (
      <div>
        {this.props.authenticated ? alreadyRegistered : form}
      </div>
    );
  }
}

Register.propTypes = {
  createUser: PropTypes.func.isRequired,
  errors: PropTypes.array,
};

const mapStateToProps = state => ({
  errors: state.user.errors,
  authenticated: state.currentUser.authenticated,
});

export default connect(mapStateToProps, { createUser })(Register);