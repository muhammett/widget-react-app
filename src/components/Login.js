import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { createSession } from '../actions/actions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const data = {
      email: this.state.email,
      password: this.state.password,
    };

    this.props.createSession(data)
  }

  render() {

    const form = (
      <div>
        {this.props.errors}

        <form onSubmit={this.onSubmit}>
          <input
            type="text"
            name="email"
            className="form-control"
            placeholder="Email"
            onChange={this.onChange}
            value={this.state.email}
          />
          <input
            type="password"
            name="password"
            className="form-control"
            placeholder="Password"
            onChange={this.onChange}
            value={this.state.password}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    )

    const alreadyLoggedIn = (
      <p> Alread Logged In </p>
    )

    return (
      <div>
        {this.props.authenticated ? alreadyLoggedIn : form}
      </div>
    );
  }
}

Login.propTypes = {
  createSession: PropTypes.func.isRequired,
  errors: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  errors: state.createSession.errors,
  authenticated: state.currentUser.authenticated,
});

export default connect(mapStateToProps, { createSession })(Login);