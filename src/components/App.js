import React, { Component } from 'react';

import Widgets from './Widgets/Widgets'


class App extends Component {
  render() {
    return (
      <Widgets />
    );
  }
}

export default App;
