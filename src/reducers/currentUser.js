import { SET_CURRENT_USER } from '../actions/types'

const initialState = {
  currentUser: {},
  authenticated: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        currentUser: action.payload,
        authenticated: Object.prototype.hasOwnProperty.call(action.payload, "id"),
      }

    default:
      return state
  }
}