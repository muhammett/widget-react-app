import { USER_REGISTER_ERROR } from '../actions/types'

const initialState = {
  errors: []
}

const createUser = (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER_ERROR:
      return {
        ...state,
        errors: action.payload
      }
    default:
      return state
  }
}

export default createUser