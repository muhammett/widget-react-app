import { WIDGET_DELETED, WIDGET_CREATED_ERROR, WIDGET_CREATED, FETCH_NEW_USER, FETCH_NEW_USER_WIDGETS } from '../actions/types'

const initialState = {
  user: {},
  widgets: [],
  widget_create_errors: [],
}

const userProfile = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEW_USER:
      return {
        ...state,
        user: action.payload,
      }
    case FETCH_NEW_USER_WIDGETS:
      return {
        ...state,
        widgets: action.payload,
      }
    case WIDGET_CREATED:
      return {
        ...state,
        widgets: [...state.widgets, action.payload]
      }
    case WIDGET_CREATED_ERROR:
      return {
        ...state,
        widget_create_errors: action.payload,
      }
    case WIDGET_DELETED:
      return {
        ...state,
        widgets: state.widgets.filter(item => item.id !== action.payload),
      }
    default:
      return state
  }
}

export default userProfile