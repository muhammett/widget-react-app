import { WIDGET_DELETED, FETCH_VISIBLE_WIDGETS } from '../actions/types'

const initialState = {
  items: []
}

const widgets = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_VISIBLE_WIDGETS:
      return {
        ...state,
        items: action.payload
      }
    case WIDGET_DELETED:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.payload),
      }
    default:
      return state
  }
}

export default widgets