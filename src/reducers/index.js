import { combineReducers } from 'redux'

import widgetReducer from './widgetReducer'

import createUser from './userReducer'

import currentUser from './currentUser'

import createSession from './sessionReducer'

import userProfile from './userProfile'

export default combineReducers({
  widgets: widgetReducer,
  user: createUser,
  currentUser: currentUser,
  createSession: createSession,
  userProfile: userProfile,
})