import { LOGIN_FAILED } from '../actions/types'

const initialState = {
  errors: []
}

const createSession = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_FAILED:
      return {
        ...state,
        errors: ['Login failed...']
      }
    default:
      return state
  }
}

export default createSession