import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

import Root from './components/Root';
import * as serviceWorker from './serviceWorker';



import store from './store';

import { fetchAndSetCurrentUser, setCurrentUser, setAuthorizationToken, fetchCurrentUser } from './actions/actions';

if (localStorage.AUTH_TOKEN) {
  console.log('AUTH_TOKEN FOUND')
  setAuthorizationToken(localStorage.AUTH_TOKEN);

  try {
    fetchCurrentUser().then(function (response) {
      store.dispatch(setCurrentUser(response.data.data))
    }).catch(function (error) {
      console.log(error);
    });
  } catch (e) {
    store.dispatch(setCurrentUser({}))
  }
}

ReactDOM.render(<Root store={store} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
